import tkinter as tk
import socket
import threading
from time import sleep
from layout_server import *

btnStart.configure(command=lambda: start_server())
btnStop.configure(command=lambda: stop_server(), state=tk.DISABLED)

server = None
HOST_ADDR = "192.168.1.13"
HOST_PORT = 5555
client_name = ""
clients = []
clients_names = []
player_data = []


# Start server function
def start_server():
    global server, HOST_ADDR, HOST_PORT  # code is fine without this
    btnStart.config(state=tk.DISABLED)
    btnStop.config(state=tk.NORMAL)

    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print(socket.AF_INET)
    print(socket.SOCK_STREAM)

    server.bind((HOST_ADDR, HOST_PORT))
    server.listen(5)  # server is listening for client connection

    threading._start_new_thread(accept_clients, (server, " "))

    lblHost["text"] = "Address: " + HOST_ADDR
    lblPort["text"] = "Port: " + str(HOST_PORT)


# Stop server function
def stop_server():
    global server
    btnStart.config(state=tk.NORMAL)
    btnStop.config(state=tk.DISABLED)


def accept_clients(the_server, y):
    while True:
        if len(clients) < 2:
            client, addr = the_server.accept()
            clients.append(client)
            print("client acceptee")

            # use a thread so as not to clog the gui thread
            threading._start_new_thread(send_receive_client_message, (client, addr))


# Function to receive message from current client AND
# Send that message to other clients
def send_receive_client_message(client_connection, client_ip_addr):
    global server, client_name, clients, player_data, player0, player1

    client_msg = " "
    # send welcome message to client
    client_name = client_connection.recv(4096)
    client_name = client_name.decode()
    if len(clients) < 2:
        client_connection.send(str.encode("welcome1"))
    else:
        client_connection.send(str.encode("welcome2"))

    clients_names.append(client_name)
    update_client_names_display(clients_names)

    if len(clients) > 1:
        sleep(1)
        symbols = ["O", "X"]

        # send opponent name and symbol
        clients[0].send(str.encode("opponent_name$" + clients_names[1] + "symbol" + symbols[0]))
        clients[1].send(str.encode("opponent_name$" + clients_names[0] + "symbol" + symbols[1]))

    while True:

        # get the player choice from received data
        data = client_connection.recv(4096)
        if not data: break

        # player x,y coordinate data. forward to the other player
        if data.startswith(b"$xy$"):
            # is the message from client1 or client2?
            if client_connection == clients[0]:
                # send the data from this player (client) to the other player (client)
                clients[1].send(data)
            else:
                # send the data from this player (client) to the other player (client)
                clients[0].send(data)

        if data.startswith(b"$message$"):
            for client in clients:
                client.send(data)

    # find the client index then remove from both lists(client name list and connection list)
    idx = get_client_index(clients, client_connection)
    del clients_names[idx]
    del clients[idx]
    client_connection.close()

    update_client_names_display(clients_names)  # update client names display


# Return the index of the current client in the list of clients
def get_client_index(client_list, curr_client):
    idx = 0
    for conn in client_list:
        if conn == curr_client:
            break
        idx = idx + 1

    return idx


# Update client name display when a new client connects OR
# When a connected client disconnects
def update_client_names_display(name_list):
    tkDisplay.config(state=tk.NORMAL)
    tkDisplay.delete('1.0', tk.END)

    for c in name_list:
        tkDisplay.insert(tk.END, c + "\n")
    tkDisplay.config(state=tk.DISABLED)


window.mainloop()
