1. Installer un éditeur (Pycharm ou VSCode)
2. Télécharger le zip
3. Ouvrir le jeu et editer les configurations (uniquement pour Pycharm)
4. Editer le fichier client et le serveur en changeant les IP et les informations de la database. (host, name, password)
5. Installer les paquets packages requis (Tkinter, mysql.connector ...)
6. Créer une base de données locale (ex : WAMP) et renseigner les champs (id, user_name, nb_win, nb_loss, last_con)


Pour lancer le jeu, il faut :


1. Lancer le serveur et appuyer sur Start afin d’établir la communication.
2. Lancer le client, taper un pseudo et appuyer sur le bouton connect. Le joueur va atterrir sur un plateau de jeu propre à lui. Il devra attendre qu’un autre joueur se connecte avant de pouvoir jouer.
3. Lancer un autre client, taper un pseudo et se connecter.
4. Pour la 1ère manche, c’est le 1er joueur qui s’est connecté qui va débuter la partie. Une fois la manche terminée, c’est le 2nd joueur qui va lancer la partie. La partie se fait donc en manche alternée.
Chaque joueur pourra discuter avec son adversaire grâce au chat qui se trouve à droite du plateau de jeu. 
