import tkinter as tk
from tkinter import *
from tkinter import messagebox
import datetime
from tkinter import PhotoImage
import socket
from time import sleep
import threading
import mysql.connector as mysql
from layout_client import *

btn_connect.configure(command=lambda: connect())
chatSend.configure(command=lambda: send_message())

# CONNECT TO BDD
try:
    db_connection = mysql.connect(host='localhost', database='tictactoe', user='root', password='')
    cursor = db_connection.cursor()

    print(db_connection)

except mysql.Error as err:
    print(err)



# NETWORK CLIENT

client = None
HOST_ADDR = "192.168.1.13"
HOST_PORT = 5555

list_labels = []
num_cols = 3
your_turn = False
you_started = False

your_details = {
    "name": "",
    "symbol": "X",
    "color": "",
    "score": 0
}

opponent_details = {
    "name": " ",
    "symbol": "O",
    "color": "",
    "score": 0
}


def connect():
    global your_details
    if len(ent_name.get()) < 1:
        tk.messagebox.showerror(title="ERROR!!!", message="You MUST enter your first name <e.g. John>")
    else:
        your_details["name"] = ent_name.get()
        print(ent_name.get())
        connect_to_server(ent_name.get())

        date = datetime.datetime.utcnow()

        req = 'INSERT INTO users(user_name, last_con) VALUES("' + ent_name.get() + '", "' \
              + date.strftime('%Y-%m-%d %H:%M:%S') + '")'
        infos = ent_name
        print(infos)

        try:
            cursor.execute(req, infos)
            db_connection.commit()

        except mysql.Error as err:
            print(err)


def connect_to_server(name):
    global client, HOST_PORT, HOST_ADDR
    try:
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client.connect((HOST_ADDR, HOST_PORT))
        print("Connection OK avec le serveur")

        client.send(str.encode(name))  # Send name to server after connecting

        # on démarre un thread pour continuer à recevoir des messages du serveur
        threading._start_new_thread(receive_message_from_server, (client, "m"))
        top_welcome_frame.pack_forget()

        top_frame.pack(side=tk.LEFT)
        chat_frame.pack(side=tk.RIGHT)
        window_main.title("Tic-Tac-Toe Client - " + name)

    except Exception as e:
        print(e)


# INITIALISATION DU PLATEAU DE JEU

for x in range(3):
    for y in range(3):
        con_frame = tk.Frame(top_frame, bg=None,
                             highlightbackground="grey",
                             highlightcolor="grey",
                             relief="solid",
                             highlightthickness=3)
        lbl = tk.Label(con_frame, text="",
                       height=2, width=5,
                       bg="#22a7f0",
                       font="Helvetica 45 bold",
                       bd=0, highlightthickness=0)
        lbl.grid(sticky='nsew')
        lbl.bind("<Button-1>", lambda e, xy=[x, y]: get_cordinate(xy))
        con_frame.grid(row=x, column=y, padx=2, pady=2)

        dict_labels = {"xy": [x, y], "symbol": "", "label": lbl, "ticked": False}
        list_labels.append(dict_labels)


# DISPLAY STATUS
def init(arg0, arg1):
    global list_labels, your_turn, your_details, opponent_details, you_started

    sleep(3)

    for i in range(len(list_labels)):
        list_labels[i]["symbol"] = ""
        list_labels[i]["ticked"] = False
        list_labels[i]["label"]["text"] = ""
        list_labels[i]["label"].config(foreground="black", highlightbackground="grey",
                                       highlightcolor="grey", highlightthickness=1)

    lbl_status.config(foreground="black")
    lbl_status["text"] = "STATUS: Game's starting."
    sleep(1)
    lbl_status["text"] = "STATUS: Game's starting.."
    sleep(1)
    lbl_status["text"] = "STATUS: Game's starting..."
    sleep(1)

    if you_started:
        you_started = False
        your_turn = False
        lbl_status["text"] = "STATUS: " + opponent_details["name"] + "'s turn!"
    else:
        you_started = True
        your_turn = True
        lbl_status["text"] = "STATUS: Your turn!"


def get_cordinate(xy):
    global client, your_turn
    # convert 2D to 1D cordinate i.e. index = x * num_cols + y
    label_index = xy[0] * num_cols + xy[1]
    label = list_labels[label_index]

    if your_turn:
        if label["ticked"] is False:
            label["label"].config(foreground=your_details["color"])
            label["label"]["text"] = your_details["symbol"]
            label["ticked"] = True
            label["symbol"] = your_details["symbol"]
            # send xy cordinate to server
            client.send(str.encode("$xy$" + str(xy[0]) + "$" + str(xy[1])))
            your_turn = False

            # Does this play leads to a win or a draw
            result = game_logic()
            if result[0] is True and result[1] != "":  # a win
                your_details["score"] = your_details["score"] + 1
                lbl_status["text"] = "Game over, You won! You(" + str(your_details["score"]) + ") - " \
                                                                                               "" + opponent_details[
                                         "name"] + "(" + str(opponent_details["score"]) + ")"
                cursor = db_connection.cursor()
                req = "UPDATE users SET nb_win = nb_win + 1 WHERE user_name = '" + your_details["name"] + "'"
                infos = your_details["name"]
                print(infos)

                try:

                    cursor.execute(req, infos)
                    db_connection.commit()

                except mysql.Error as err:
                    print(err)

                lbl_status.config(foreground="green")
                threading._start_new_thread(init, ("", ""))

            elif result[0] is True and result[1] == "":  # a draw
                lbl_status["text"] = "Game over, Draw! You(" + str(your_details["score"]) + ") - " \
                                                                                            "" + opponent_details[
                                         "name"] + "(" + str(opponent_details["score"]) + ")"
                lbl_status.config(foreground="blue")
                threading._start_new_thread(init, ("", ""))

            else:
                lbl_status["text"] = "STATUS: " + opponent_details["name"] + "'s turn!"
    else:
        lbl_status["text"] = "STATUS: Wait for your turn!"
        lbl_status.config(foreground="red")

        # send xy coordinate to server to server


# [(0,0) -> (0,1) -> (0,2)], [(1,0) -> (1,1) -> (1,2)], [(2,0), (2,1), (2,2)]
def check_row():
    list_symbols = []
    list_labels_temp = []
    winner = False
    win_symbol = ""
    for i in range(len(list_labels)):
        list_symbols.append(list_labels[i]["symbol"])
        list_labels_temp.append(list_labels[i])
        if (i + 1) % 3 == 0:
            if list_symbols[0] == list_symbols[1] == list_symbols[2]:
                if list_symbols[0] != "":
                    winner = True
                    win_symbol = list_symbols[0]

                    list_labels_temp[0]["label"].config(foreground="#00e640", highlightbackground="green",
                                                        highlightcolor="green")
                    list_labels_temp[1]["label"].config(foreground="#00e640", highlightbackground="green",
                                                        highlightcolor="green")
                    list_labels_temp[2]["label"].config(foreground="#00e640", highlightbackground="green",
                                                        highlightcolor="green")

            list_symbols = []
            list_labels_temp = []

    return [winner, win_symbol]


# [(0,0) -> (1,0) -> (2,0)], [(0,1) -> (1,1) -> (2,1)], [(0,2), (1,2), (2,2)]
def check_col():
    winner = False
    win_symbol = ""
    for i in range(num_cols):
        if list_labels[i]["symbol"] == list_labels[i + num_cols]["symbol"] == list_labels[i + num_cols + num_cols][
            "symbol"]:
            if list_labels[i]["symbol"] != "":
                winner = True
                win_symbol = list_labels[i]["symbol"]
                print("winner")
                list_labels[i]["label"].config(foreground="green", highlightbackground="green",
                                               highlightcolor="green")
                list_labels[i + num_cols]["label"].config(foreground="green", highlightbackground="green",
                                                          highlightcolor="green")
                list_labels[i + num_cols + num_cols]["label"].config(foreground="green", highlightbackground="green",
                                                                     highlightcolor="green")

    return [winner, win_symbol]


def check_diagonal():
    winner = False
    win_symbol = ""
    i = 0
    j = 2

    # top-left to bottom-right diagonal (0, 0) -> (1,1) -> (2, 2)
    a = list_labels[i]["symbol"]
    b = list_labels[i + (num_cols + 1)]["symbol"]
    c = list_labels[(num_cols + num_cols) + (i + 1)]["symbol"]
    if list_labels[i]["symbol"] == list_labels[i + (num_cols + 1)]["symbol"] == \
            list_labels[(num_cols + num_cols) + (i + 2)]["symbol"]:
        if list_labels[i]["symbol"] != "":
            winner = True
            win_symbol = list_labels[i]["symbol"]

            list_labels[i]["label"].config(foreground="green", highlightbackground="green",
                                           highlightcolor="green")

            list_labels[i + (num_cols + 1)]["label"].config(foreground="green", highlightbackground="green",
                                                            highlightcolor="green")
            list_labels[(num_cols + num_cols) + (i + 2)]["label"].config(foreground="green",
                                                                         highlightbackground="green",
                                                                         highlightcolor="green")

    # top-right to bottom-left diagonal (0, 0) -> (1,1) -> (2, 2)
    elif list_labels[j]["symbol"] == list_labels[j + (num_cols - 1)]["symbol"] == list_labels[j + (num_cols + 1)][
        "symbol"]:
        if list_labels[j]["symbol"] != "":
            winner = True
            win_symbol = list_labels[j]["symbol"]

            list_labels[j]["label"].config(foreground="green", highlightbackground="green",
                                           highlightcolor="green")
            list_labels[j + (num_cols - 1)]["label"].config(foreground="green", highlightbackground="green",
                                                            highlightcolor="green")
            list_labels[j + (num_cols + 1)]["label"].config(foreground="green", highlightbackground="green",
                                                            highlightcolor="green")
    else:
        winner = False

    return [winner, win_symbol]


# it's a draw if grid is filled
def check_draw():
    for i in range(len(list_labels)):
        if list_labels[i]["ticked"] is False:
            return [False, ""]
    return [True, ""]


def game_logic():
    result = check_row()
    if result[0]:
        return result

    result = check_col()
    if result[0]:
        return result

    result = check_diagonal()
    if result[0]:
        return result

    result = check_draw()
    return result


def receive_message_from_server(sck, m):
    global your_details, opponent_details, your_turn, you_started
    while True:
        from_server = sck.recv(4096)
        from_server = from_server.decode()

        if not from_server: break

        if from_server.startswith("welcome"):
            if from_server == "welcome1":
                your_details["color"] = "white"
                opponent_details["color"] = "#545454"
                lbl_status["text"] = "Server: Welcome " + your_details["name"] + "! Waiting for player 2"
            elif from_server == "welcome2":
                lbl_status["text"] = "Server: Welcome " + your_details["name"] + "! Game will start soon"
                your_details["color"] = "#545454"
                opponent_details["color"] = "white"

        elif from_server.startswith("opponent_name$"):
            temp = from_server.replace("opponent_name$", "")
            temp = temp.replace("symbol", "")
            name_index = temp.find("$")
            symbol_index = temp.rfind("$")
            opponent_details["name"] = temp[0:name_index]
            your_details["symbol"] = temp[symbol_index:len(temp)]

            # set opponent symbol
            if your_details["symbol"] == "O":
                opponent_details["symbol"] = "X"
            else:
                opponent_details["symbol"] = "O"

            lbl_status["text"] = "STATUS: " + opponent_details["name"] + " is connected!"
            sleep(3)
            # is it your turn to play? hey! 'O' comes before 'X'
            if your_details["symbol"] == "O":
                lbl_status["text"] = "STATUS: Your turn!"
                your_turn = True
                you_started = True
            else:
                lbl_status["text"] = "STATUS: " + opponent_details["name"] + "'s turn!"
                you_started = False
                your_turn = False
        elif from_server.startswith("$xy$"):
            temp = from_server.replace("$xy$", "")
            _x = temp[0:temp.find("$")]
            _y = temp[temp.find("$") + 1:len(temp)]

            # update board
            label_index = int(_x) * num_cols + int(_y)
            label = list_labels[label_index]
            label["symbol"] = opponent_details["symbol"]
            label["label"]["text"] = opponent_details["symbol"]
            label["label"].config(foreground=opponent_details["color"])
            label["ticked"] = True

            # Does this cordinate leads to a win or a draw
            result = game_logic()
            if result[0] is True and result[1] != "":  # opponent win
                opponent_details["score"] = opponent_details["score"] + 1
                if result[1] == opponent_details["symbol"]:  #
                    lbl_status["text"] = "Game over, You Lost! You(" + str(your_details["score"]) + ") - " \
                                                                                                    "" + \
                                         opponent_details["name"] + "(" + str(opponent_details["score"]) + ")"
                    lbl_status.config(foreground="red")
                    threading._start_new_thread(init, ("", ""))

                    cursor = db_connection.cursor()
                    req = "UPDATE users SET nb_loss = nb_loss + 1 WHERE user_name = '" + your_details["name"] + "'"
                    infos = your_details["name"]
                    print(infos)

                    try:
                        cursor.execute(req, infos)
                        db_connection.commit()

                    except mysql.Error as err:
                        print(err)

            elif result[0] is True and result[1] == "":  # a draw
                lbl_status["text"] = "Game over, Draw! You(" + str(your_details["score"]) + ") - " \
                                                                                            "" + opponent_details[
                                         "name"] + "(" + str(opponent_details["score"]) + ")"
                lbl_status.config(foreground="blue")
                threading._start_new_thread(init, ("", ""))
            else:
                your_turn = True
                lbl_status["text"] = "STATUS: Your turn!"
                lbl_status.config(foreground="black")
        elif from_server.startswith("$message$"):
            message = from_server.replace("$message$", "")
            messageLabel = tk.Label(chatContainer, text=message, bg="white", wraplength=300, anchor=SW)
            messageLabel.pack(side=tk.TOP, fill='both')

    sck.close()


# ENVOI DE MESSAGE DU TCHAT
def send_message():
    client.send(str.encode("$message$" + str(ent_name.get()) + " > " + str(chatInput.get())))
    chatInput.delete(0, "end")


window_main.mainloop()
