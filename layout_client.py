import tkinter as tk

# MAIN GAME WINDOW
window_main = tk.Tk()
window_main.title("Tic-Tac-Toe Client")
window_main.iconbitmap("logo.ico")

top_welcome_frame = tk.Frame(window_main)
lbl_name = tk.Label(top_welcome_frame, text="Name:")
lbl_name.pack(side=tk.LEFT)
ent_name = tk.Entry(top_welcome_frame)
ent_name.pack(side=tk.LEFT)
btn_connect = tk.Button(top_welcome_frame, text="Connect")
btn_connect.pack(side=tk.LEFT)
top_welcome_frame.pack(side=tk.TOP)

top_frame = tk.Frame(window_main)

# CHAT WINDOW

chat_frame = tk.Frame(window_main, width=200, height=500)
# chat_frame.resizable(chat_frame, width=None, height=None)
chatInput = tk.Entry(chat_frame)
chatInput.pack(side=tk.BOTTOM, padx=5, pady=5, fill=tk.X)
chatSend = tk.Button(chat_frame, text="Send")
chatSend.pack(side=tk.BOTTOM, padx=5, pady=5, fill=tk.X)
chatContainer = tk.Frame(chat_frame, bg="white")
chatContainer.pack(side=tk.TOP, padx=5, pady=5, fill=tk.BOTH)

lbl_status = tk.Label(top_frame, text="Status: Not connected to server", font="Helvetica 14 bold")
lbl_status.grid(row=3, columnspan=3)

top_frame.pack_forget()
chat_frame.pack_forget()